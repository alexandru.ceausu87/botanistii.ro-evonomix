document.addEventListener('DOMContentLoaded', () => {
    document.querySelectorAll('ul.level0.submenu').forEach(element => {
        const link = element.previousElementSibling.getAttribute('href');
        element.innerHTML += `<a href="${link}" class="category-link">Vezi toate produsele</a>`;
    });
});