document.addEventListener('scroll', function() {

    const megamenu = document.querySelector('.megamenu');
    const search = document.querySelector('.block.block-search');
    const headerSearch = document.querySelector('.header-search');
    const stickySearch = document.querySelector('.sticky-search');
    const stickyHidden = document.querySelectorAll('.sticky-hidden');
    const navigation = document.querySelector('.sections.nav-sections');
    const products = document.querySelector('.megamenu-products-dropdown');
    const navContainer = document.querySelector('.nav-container');
    const sliderContainer = document.querySelector('.slider-container');


    if(window.scrollY >= 200) {
        megamenu.classList.add('fixed_header');
        stickySearch.appendChild(search);
        stickyHidden.forEach((element) => element.style.display = 'none');
        navigation.style.display = 'none';
        products.appendChild(navigation);
        
        products.addEventListener('mouseenter', () => {
            if(megamenu.classList.contains('fixed_header')) {
                navigation.style.display = 'block';
                navigation.style.marginTop = '8px';
            }
        });

        products.addEventListener('mouseleave', () => {
            if(megamenu.classList.contains('fixed_header')) {
                navigation.style.display = 'none';
                navigation.style.marginTop = '0';
            }
        });

        
    } else {
        megamenu.classList.remove('fixed_header');
        headerSearch.appendChild(search);
        stickyHidden.forEach((element) => element.style.display = 'inline');
        navigation.style.display = 'block';
        navContainer.appendChild(navigation);
    }
});